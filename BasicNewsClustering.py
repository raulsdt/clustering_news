import os, numpy
import nltk
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics.cluster import adjusted_rand_score
from ProcessText import TextProcessor

def cluster_texts(texts, clustersNumber, distance):
    #Load the list of texts into a TextCollection object.


    collection = nltk.TextCollection(texts)
    print("Created a collection of", len(collection), "terms.")

    #get a list of unique terms
    unique_terms = list(set(collection))
    print("Unique terms found: ", len(unique_terms))


    print('unique terms ', unique_terms)

    ### And here we actually call the function and create our array of vectors.
    vectors = [numpy.array(TFIDF(f,unique_terms, collection)) for f in texts]
    print("Vectors created.")

    print(vectors)
    # initialize the clusterer
    clusterer = AgglomerativeClustering(n_clusters=clustersNumber,
                                      linkage="average", affinity=distanceFunction)
    clusters = clusterer.fit_predict(vectors)

    return clusters


def isNamedEntity(entities_names, word):
    for entity_name in entities_names:
        if(word in entity_name.lower()):
            return True

    return False


# Function to create a TF vector for one document. For each of
# our unique words, we have a feature which is the tf/idf for that word
# in the current document
def TFIDF(document, unique_terms, collection):
    #print(len(final_entities_names))
    word_tf = []
    for word in unique_terms:
            word_tf.append(collection.tf_idf(word, document))
    return word_tf

if __name__ == "__main__":
    folder = "CorpusTXT"
    # Empty list to hold text documents.
    texts = []
    final_entities_names = []

    listing = os.listdir(folder)
    p = TextProcessor()
    for file in listing:
        #print("File: ",file)
        if file.endswith(".txt"):
            url = folder+"/"+file

            f = open(url,encoding="latin-1");
            raw = str(f.read())
            f.close()

            final_text = p.process_text(raw,file)
            texts.append(final_text)

    print("Prepared ", len(texts), " documents...")
    print("They can be accessed using texts[0] - texts[" + str(len(texts)-1) + "]")

    distanceFunction ="cosine"
    #distanceFunction = "euclidean"
    test = cluster_texts(texts,5,distanceFunction)
    print("test: ", test)

    # Gold Standard
    reference = ['cataluña','avion','cataluña','Oxfam','cataluña','avion','vaticano','avion','Oxfam','merkel','avion',
                 'Oxfam','merkel','vaticano','merkel','merkel','merkel','cataluña','avion','avion','avion','Oxfam' ]
    print("reference: ", reference)

    # Evaluation
    print("rand_score: ", adjusted_rand_score(reference,test))

