from bs4 import BeautifulSoup
from textblob import TextBlob
import os


class HtmlToTxt(object):

    def process(self, fileName):
        with open('CorpusHTML/' + fileName, 'rb') as f:
            print('Processing: ' + fileName)
            bsObj = BeautifulSoup(f.read(), 'html.parser')
            with open('CorpusTXT/' + fileName[:-4]+'txt', 'w') as fOutput:
                for bs_clean in bsObj.findAll(["script", "style"]):
                    bs_clean.extract()

                for p_tag in bsObj.findAll('p'):
                    if(len(p_tag.text) > 3):
                        t = TextBlob(p_tag.text)

                        if (t.detect_language() == 'es'):
                            raw_translate = t.translate(to='en')
                        else:
                            raw_translate = p_tag.text
                        print(raw_translate.replace('’’','"').replace('’','\''), file=fOutput)







if __name__ == "__main__":

    htmlToTxt = HtmlToTxt()
    folder = "CorpusHTML"
    # Empty list to hold text documents.
    texts = []

    listing = os.listdir(folder)
    for filename in listing:
        # print("File: ",file)
        if filename.endswith(".html"):

            htmlToTxt.process(filename)