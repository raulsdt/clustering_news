from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem.porter import *
from nltk.corpus import stopwords
from nltk.corpus import wordnet
import nltk
import string
from nltk.stem.porter import *


class TextProcessor():
    # Se obtienen las stopwords del inglés y se crea un lematizador.
    def __init__(self):
        self.stop = set(stopwords.words('english'))
        self.lemmatizer = WordNetLemmatizer()
        self.stemmer = PorterStemmer()
        self.grammar = '\n'.join([
            # 'NP: {<NNP><VB>*<VBD>*<VBG>*<VBN>*<VBP>*<VBZ>}',
            # 'NP: {<JJ>*<NNP>}',
            # 'NP: {<NNP>}',

            # 'NP: { <WDT>*<VBZ><NNP>}',
            # 'NP: { <WDT>*<VBN><NNP>}',

            # 'NP: { <WDT>*<VBD><NNP>}',

            #'NP: { <WDT>*<VB><NNP>}',


            #'NP: {<JJ><NNP>}',
            #'NP: {<JJ><NN>}',

            #'NP: {<NNP>}',

            #'NP: {<IN><NNP>}',
            #'NP: {<NN><IN>}',

            #'NP: {<NNP><NNP><NNP>}',
            'NP: {<NNP><NNP><IN><NNP><NNP>}',
            # 'NP: {<NN><NNP><NNP>}',
            #'NP: {<IN>*<NNS><IN>*}',
            #'NP: {<JJ><NN>}',
            #'NP: {<IN><NN>}'
            # 'NP: {<VBD><NNP>}',

            # 'NP: { <VBD><NNP><JJ>}',
            # 'NP: { <TO><NNP>}',
            # 'NP: { <WDT><VBD><DT><VBD><NNP>}',
            # 'NP: { <IN><NNP>}',
            # 'NP: {<NNP><VBD>}',
            # 'NP: { <VB><NNP>}',
            # 'NP: {<VBD><NNP>}',

            # 'NP: {<IN><DT><NN>}',
            # 'NP: {<VBD><NNP>}',
            # 'NP: {<NNP><NN><VBD>}',

            # 'NP: {<IN><DT><NNP>}',
            # 'NP: {<TO><DT><NNP>}',

            # 'NP: {<NNP>}',

            # 'NP: {<VB><NN>}',

            # 'NP: {<WDT><NNP><VB>}',
            # 'NP: {<NNP><JJ>}',
            # 'NP: {<JJ><NNP>}',

            # 'NP: { <WDT><DT><NN>}',

            # 'NP: {<NN>}',
            # 'NP: {<NNP>}',
        ])

    # Se filtran las stopwords del texto.
    def filter_stopwords(self, token_list):
        clean_tokens = []
        for token in token_list:
            if token[0] not in self.stop:
                clean_tokens.append(token)
        return clean_tokens

    # Se eliminan los símbolos de puntuación.
    def remove_punctuation(self, token_list):
        result = []
        for token in token_list:
            punct_removed = ''.join([letter for letter in token if letter in string.ascii_letters])
            if punct_removed != '':
                result.append(punct_removed)
        return result

    # Se genera la equivalencia para que el lematizador entienda los tags de la NLTK.
    def wordnet_value(self, value):
        result = ''
        if value.startswith('J'):
            return wordnet.ADJ
        elif value.startswith('V'):
            return wordnet.VERB
        elif value.startswith('N'):
            return wordnet.NOUN
        elif value.startswith('R'):
            return wordnet.ADV
        return result

    # Se realiza la lematización de los tokens conforme a su tag modificado.
    def lemmatize(self, token_list):
        result = []
        for token in token_list:
            if len(token) > 0:
                pos = self.wordnet_value(token[1])
                if pos != '':
                    result.append(self.lemmatizer.lemmatize(str(token[0]).lower(), pos=pos))
        return result

    def stemming(self,token_list):
        result = []
        for token in token_list:
            result.append(self.stemmer.stem(token))
        return result

    # Función recursiva que recorre el arbol.
    def extract_entity_names(self, t):
        entity_names = []
        # Se comprueba que el token tenga etiqueta.
        if hasattr(t, 'label') and t.label:
            # Si es un entity name entonces lo agregamos con los que ya hemos identificado.
            if t.label() == 'GPE' or t.label() == 'PERSON':
                entrada = ' '.join([child[0] if child[0] not in ['Francis', 'Pope','Catholic'] else '' for child in t])
                if(entrada not in ('',' ')):
                    entity_names.append(entrada)
            # En caso contrario obtenemos todos los hijos del token para continuar con la búsqueda.
            else:
                for child in t:
                    entity_names.extend(self.extract_entity_names(child))
        return entity_names

    def chunk(self, pos_tags):
        chunkparser = nltk.RegexpParser(self.grammar)
        return chunkparser.parse(pos_tags)

    # Se procesa un texto completo, eliminando stopwords, símbolos de puntuación, y retornando una lista de tokens
    # lematizados en minúsculas.
    def process_text(self, text, file):
        with open("tag/struct_" + file + ".txt", "w") as outputfile:
            tokens = nltk.word_tokenize(text)
            tokens = self.filter_stopwords(tokens)
            tokens = self.remove_punctuation(tokens)

            # Traductor propio
            for idx, val in enumerate(tokens):
                if "Francisco" in val:
                    tokens[idx] = 'Francis'

            tokens = nltk.pos_tag(tokens)

            outputfile.write(str(tokens))
            outputfile.close()

            # Filtro por GPE y PERSON
            text_chunk = nltk.ne_chunk(tokens)

            token_chunks = self.extract_entity_names(text_chunk)

            # Filtro por NRE

            #text_chunk = self.chunk(tokens)
            #for subtree in text_chunk.subtrees(filter=lambda t: t.label() == 'NP'):

            #    token_chunks = token_chunks + self.lemmatize(subtree)

            print(file + ' tokens_chunks ', token_chunks)
            token_chunks=self.stemming(token_chunks)

            return token_chunks